Notes app
=========


1. Installation (node modules)
----
> npm install

> npm install gulp -g


2. Generate css & js 
----
> gulp


3. Start
----

> npm start


4. Load sample data
----

> Open: http://localhost:3000/db

Structure of a Project
----
/views
HTML template files for frontend

/public/assets/css
Css / less files

/public/assets/js
Javascript files for frontend logic

/server
Javascript files for backend logic

/build
Pre-generated javascript & css files

gulpfile.js
Configuration for creating pre-generated javascript files & libraries

server.js
Application starter


Frontend libraries/helpers
----
- jQuery - Selectors, Events, CSS
- jQuery UI - Draggable, Dropable
- Socket.IO - sending/receiving data
- Backbone
- normalize.css

Backend libraries
----
- express - web
- underscore - templates
- nedb - database storage
- Socket.IO
- gulp - minifying javascript & compiling less
