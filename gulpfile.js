'use strict';

//var path = require('path');
var gulp = require('gulp');
var gutil = require('gulp-util');
var clean = require('gulp-clean');
//var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var less = require('gulp-less');
var mocha = require('gulp-mocha');
var watch = require('gulp-watch');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var streamify  = require('gulp-streamify');

// config paths
var paths = {
	'build': './build/',
	'scripts': './public/js/*.js',
	'script': './public/js/main.js',
	'less': './public/css/style.less'
};


// default task
gulp.task('default', ['browserify', 'css']);


/** remove ./build folder */
gulp.task('clean', function () {
	return gulp.src( paths.build , {read: false})
		.pipe( clean() )
		.on('error', gutil.log);
});

/** generate styl.ecss file */
gulp.task('css', function () {
	return gulp.src( paths.less )
		.pipe( less({
//			paths: [ path.join(__dirname, 'node_modules', 'normalize.css') ]
			paths: './node_modules/normalize.css'
			, compress: true
		}) )
		.pipe( gulp.dest( paths.build ) )
		.on('error', gutil.log);
});

/** generate app.js file */
gulp.task('browserify', function() {
	return browserify({
			entries: paths.script
		})
		.bundle()
		.on('error', gutil.log)
		.pipe( source( 'app.js' ) )
		.pipe( streamify(uglify()) )
		.pipe( gulp.dest( paths.build ) );
});

/** generate on change */
gulp.task('watch', function () {
	gulp.watch( paths.scripts, ['browserify']);
});

/** run tests */
gulp.task('test', function() {
	return gulp.src(['test/test-*.js'], {read: false})
		.pipe(mocha({
			reporter: 'spec',
			globals: {
				should: require('should')
			}
		}));
});