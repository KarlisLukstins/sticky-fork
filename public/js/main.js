'use strict';

var socket = require('socket.io-client')();
require('jquery-ui');
var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

$(function(){
	var noteModel = Backbone.Model.extend({
		defaults: { id:null, content: null, x: null, y: null },
		isImage: function(){ return !!this.get('image'); }
	});

	var noteView = Backbone.View.extend({
		events:{
			'dblclick': 'makeEditable',
			'blur span': 'blur'
		},

		initialize: function(){
//			console.log('view:noteView', 'initialize');
		},

		render: function() {
//			console.log('view:noteView', 'render');
			var noteClass, noteContent;
			if( this.model.isImage() ){
				noteClass = 'noteImg';
				noteContent = 'image url here';
			} else {
				noteClass = 'noteTxt';
				noteContent = 'enter your text';
			}
			var $note = $(this.el);
			$note.css('position', 'absolute');
			$note.addClass('note');
			$note.addClass(noteClass);
			var content = this.model.get('content') || noteContent;
			if( this.model.isImage() && this.checkImageUrlContent( content ) ){
				$note.css('background-image', 'url(' + content + ')').addClass('done');
			} else {
				$note.html('<span>' + content + '</span>');
			}
//			console.log( this.model.cid, this.cid);
//			$note.attr( 'data-view', this );
			$note.data('view', this);
			$('#notes').append( this.el );

			this.bindView();
			this.updateView();
			return this;
		},

		updateView: function(){
//			console.log('view:noteView', 'updateView');
			var $note = $(this.el);
			$note.css({
				left: this.model.get('x'),
				top: this.model.get('y')
			});
		},

		bindView: function(){
			var _this = this;
			// bind dragging
			$( this.el ).draggable({
				stack: ".ui-draggable",
				cursor: "crosshair",
				stop: function( event, ui ){
					_this.move(ui);
				}
			});
		},

		blur: function(){
			console.log('view:noteView', 'blur');
			var $el = $('span', this.el);
			var text = $el.text();
			this.update( text );

			// check for URL content , only for image type
			if( ! this.model.isImage() ){
				return;
			}
			if( this.checkImageUrlContent(text) ){
				$( this.el ).css('background-image', 'url(' + text + ')').addClass('done');
				$el.text('');
			} else {
				console.info('note image, bad url');
			}
		},

		checkImageUrlContent: function( text ){
			var regexUrl = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi);
			return text.match(regexUrl);
		},

		makeEditable: function(){
			console.log('view:noteView', 'makeEditable');
			var $el = $('span', this.el);
			var txt = $el.text();
			if( txt == 'image url here' || txt == 'enter your text' ){
				$el.text('');
			}
			$el.attr('contentEditable', true).focus();
		},

		move: function( ui ){
			console.log('view:noteView', 'move', ui);
			var data = {
				id: this.id,
				x: ui.position.left,
				y: ui.position.top
			};
			socket.emit('move', data);
		},

		update: function( content ){
			console.log('view:noteView', 'update', this.id, content);
			var data = {
				id: this.id,
				content: content
			};
			socket.emit('update', data);
		},

		delete: function(){
			console.log('view:noteView', 'delete', this);
			var data = {
				id: this.id
			};
			socket.emit('delete', data);
			this.el.remove();
			this.model.collection.remove( this.model );
			return false;
		}
	});

	var notesCollection = Backbone.Collection.extend({ model: noteModel });
	var notes = new notesCollection();
	var notesView = Backbone.View.extend({
		views: {},
		initialize: function(){
			this.collection.bind('add', function(model) {
//				console.log( 'model add', model );
				this.views[ model.cid ] = new noteView({ model: model, id: model.id, cid: model.cid }).render();
			}, this);
		}
	});
	var notesView = new notesView({ collection: notes });


	var desktopModel = Backbone.Model.extend({ defaults:{ id: null, name: null, selected: null } });
	var desktopsCollection = Backbone.Collection.extend({ model: desktopModel });
	var desktops = new desktopsCollection();
	var DesktopView = Backbone.View.extend({
		events: {
			'click': 'onDesktop'
		},
		onDesktop: function( e ){
			console.log( this.model );
			console.log('DesktopView:onDesktop', this.model.id );
			// reset active desktops
			this.model.collection.each(function( desktop ){
				desktop.set('selected', false)
			});
			this.model.set('selected', true);
			socket.emit('loadNotes', { id: this.model.id });
			this.updateView();
		},
		render: function(){
			this.$el.html('<a><span></span>'+ this.model.get('name') +'</a>' ) ;
			$('#desktopSelector').append( this.$el );
			this.updateView();
		},
		updateView: function(){
//			console.log('updateView:onDesktop', this );
			if( this.model.get('selected') ) {
				$('#desktopSelector a').removeClass('active');
				var $desktop = $('a', this.$el);
				$desktop.addClass('active');
			}
		}
	});
	var DesktopsView = Backbone.View.extend({
		el: 'body',
		events: {
			'click #addDesktop': 'addDesktop',
			'click #noteTxt': 'addNoteTxt',
			'click #noteImg': 'addNoteImg'
		},
		addDesktop: function(){
			console.info('click', 'addDesktop');
			socket.emit('desktopAdd', true);
		},
		addNoteTxt: function(){
			console.info('click', 'note txt');
			this.addNote('txt');
		},
		addNoteImg: function(){
			console.info('click', 'note img');
			this.addNote('img');
		},
		addNote: function( type ){
			console.log('DesktopsView:addNote (type)', type);
			var _desktopId = null;
			// look for selected
			this.collection.each(function( desktop ){
				console.log( 'selected', desktop.get('selected') );
				if( desktop.get('selected') ){
					_desktopId = desktop.id;
					return false;
				}
			});
			if( !_desktopId ){
				console.log( 'no active desktop ');
				_desktopId = this.setActiveDesktop();
				if( !_desktopId ){
					return;
				}
			}
			console.log('_desktopId', _desktopId );

			var data = {
				id: _desktopId
			};
			if( type == 'img' ){
				data.image = true;
			}
			socket.emit('create', data);
		},
		render: function(){
			$('#desktopSelector').empty();
			this.setActiveDesktop();
			this.collection.each(function( desktop ){
				var desktopView = new DesktopView({ model: desktop });
				desktopView.render();
			});
		},
		setActiveDesktop: function(){
			// set first active desktop
			if( this.collection.length ) {
				var _select = this.collection.first();
				_select.set('selected', true);
				return _select.id;
			}

		},
		loadNotes: function() {
			if( ! this.collection.length ){
				console.log( 'no desktops' );
				return;
			}
			var _desktop = this.collection.first();
			socket.emit('loadNotes', { id: _desktop.id });
		}
	});
	var desktopsView = new DesktopsView({ collection: desktops });


	/* onload request data */
	socket.emit('loadDesktops', true);
	socket.on('loadDesktops', function( res ){
		console.log('io:loadDesktops', res);
		for( var i = 0, l = res.length; i < l ; i++){
			var _data = {
				id: res[i]._id,
				name: res[i].name
			};
			desktops.add( _data );
		}
		desktopsView.render();
		desktopsView.loadNotes();
	});


	/* bind elements */
	$("#bin").droppable({
		accept: ".note",
		activeClass: "active",
		hoverClass: "hover",
		tolerance: "pointer",
		drop: function( event, ui ){
			console.log( 'droppable view', $(ui.draggable).data('view') );
			$( ui.draggable ).data('view').delete();
		}
	});


	socket.on('loadNotes', function( res ){
		// clean
		notes.reset();
		$('#notes').empty();

		console.log('io:loadNotes', res );
		for( var i = 0, l = res.length; i < l ; i++){
			var _data = res[i].data;
			_data.id = res[i]._id;
			notes.add( _data );
		}
	});

	socket.on('noteCreate', function( data ){
		console.log('io:noteCreate', data );
		data.data.id = data._id;
		notes.add( data.data );
	});

	socket.on('desktopCreate', function( data ){
		console.log('io:desktopCreate', data );
		data.id = data._id;
		desktops.add( data );
		desktopsView.render();
	});

	socket.on('deleted', function( data ){
		console.info('io:deleted (only info notice)', data );
	});

	socket.on('moved', function( data ){
		console.info('io:moved (only info notice)', data );
	});

});



