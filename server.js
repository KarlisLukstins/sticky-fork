'use strict';

var express = require('express');
var app = express();
var compression = require('compression');
var http = require('http').Server( app );
var io = require('socket.io')( http );

app.use(compression());
// config express views
app.set('views', __dirname + '/views');
app.set('view engine', 'tmpl');
// remove headers
app.disable('x-powered-by');

require('underscore-express')( app );
require('./server/routes')( app , express );
require('./server/socket')( io );


var port = process.env.PORT || 3000;
http.listen(port, function(){
	console.log('listening on *:'+ port );
});