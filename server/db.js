'use strict';

var Datastore = require('nedb');
var db = new Datastore({ filename: './build/db.json', autoload: true });

/** Database reference */
module.exports = db;