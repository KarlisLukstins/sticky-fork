'use strict';

var _ = require('underscore');

/* helper functions */
/** Retrieve desktop id From desktop list By desktop order number */
exports.getDesktopId = function( desktops, desktopNum ){
	for( var i = 0, l = desktops.length; i < l; ++i){
		var desktop = desktops[i];
		if( desktop.order === desktopNum ){
			return desktop._id;
		}
	}
};

/** Generate notes HTML for static page */
exports.generateNotes = function( notes, page ){
	// ToDo use one template with client side
	var _note = _.template('<div id="<%= id %>" class="note <%= type %> <%= (bg ? \'done\' : \'\') %>"'
	+' style="top:<%= y %>px;left:<%= x %>px;<%= bg %>">'
		+'<span><%= content %></span>'
	+'</div>');

	var html = '';
	for( var i = 0, l = notes.length; i < l; ++i) {
		var note = notes[i];
		if (note.desktopId !== page) {
			continue;
		}
		var content = '';
		var bg = '';
		if( !note.data.image && note.data.content !== '' && note.data.content !== undefined ){
			content = note.data.content;
		}else if( note.data.image && note.data.content !== '' && note.data.content !== undefined ){
			content = '';
			bg = 'background-image:url('+ note.data.content +')';
		}else{
			content = ( note.data.image ? 'image url here' : 'enter your text' );
		}
		html += _note({
			id: note._id,
			type: 'note'+ ( note.data.image ? 'Img' : 'Txt' ),
			y: note.data.y,
			x: note.data.x,
			bg: bg,
			content: content
		});
	}
	return html;
};

/** Get predefined desktop name */
exports.getDesktopName = function( number ){
	var name = {
		'0': 'One',
		'1': 'Two',
		'2': 'Three',
		'3': 'Four',
		'4': 'Five',
		'5': 'Six',
		'6': 'Seven',
		'7': 'Eight',
		'8': 'Nine',
		'9': 'Ten'
	};
	return 'Desktop '+ ( typeof( name[number] ) !== 'undefined' ? name[number] : '#'+ number );
};