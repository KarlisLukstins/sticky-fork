'use strict';

var db = require('./db');
var helper = require('./helper');

/** Preloads data for first time usage */
exports.preloadDB = function( req, res ){
	db.remove({}, {multi:true}, function (err, numRemoved){
		var desktops = [
			{'type': 'desktop', name: helper.getDesktopName(0), order: 0},
			{'type': 'desktop', name: helper.getDesktopName(1), order: 1}
		];
		db.insert(desktops, function (err, doc) {
			var doc = doc[0];
			if (doc.order === 0) {
				var _id = doc._id;
				var _order = doc.order;
				var notes = [
					{desktopId: _id, 'type': 'note', data: {image: true, x: 50, y: 50}},
					{desktopId: _id, 'type': 'note', data: { x: 100, y: 100}}
				];
				db.insert(notes, function (err, doc){});
			}
		});
		res.send('done');
	});
};

/** Load and Render data for static page (no javascript support) */
exports.loadStatic = function( req, res ){
	var desktopNum = parseInt(req.params.id) || 0;
	db.find({ type: 'desktop' }).sort({ order: 1 }).exec(function( err, desktopsAll ){
		db.find({ type: 'note' }, function(err, notesAll){
			var desktopId = helper.getDesktopId( desktopsAll, desktopNum);
			var notes = helper.generateNotes( notesAll, desktopId );

			res.render('main', {
				static: true,
				desk: desktopNum,
				desktopId: desktopId,
				desktops: desktopsAll,
				notes: notes
			});
		});
	});
};

/** Render main page */
exports.load = function( req, res ){
	var desktopNum = parseInt(req.params.id) || 0;
	res.render('main', {
		desk: desktopNum
	});
};

/** Load data, return JSON */
/*
exports.loadJSON = function( req, res ){
	var desktopNum = parseInt(req.params.id) || 0;
	db.find({ type: 'desktop' }, function(err, desktopsAll){
		db.find({ type: 'note' }, function(err, notesAll){
			var desktopId = helper.getDesktopId( desktopsAll, desktopNum);
			var notes = helper.generateNotes( notesAll, desktopId );

			console.log(notes);
			return notes;
		});
	});
};
*/

/** Render error page */
exports.notfound = function( req, res ){
	res.status(404);
	res.send('Not Found');
};