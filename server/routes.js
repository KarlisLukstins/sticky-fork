'use strict';

module.exports = function( app , express ){

	// define static assets location
	app.use('/assets/images', express.static(__dirname + '/../public/images'));
	app.use('/assets', express.static(__dirname + '/../build'));

	var pages = require('./pages');

	app.get('/db', pages.preloadDB);

	app.get('/static', pages.loadStatic);
	app.get('/static/:id', pages.loadStatic);

	app.get('/', pages.load);
	app.get('*', pages.notfound);
};