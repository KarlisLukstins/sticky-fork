'use strict';

var db = require('./db');
var helper = require('./helper');

/** Server requests & responses for socket.io */
module.exports = function( io ){

	io.on('connection', function( socket ){

		// move note
		socket.on('move', function( data ){
			console.log('io:move', data );
			db.update({ _id: data.id }, {$set: {"data.x": data.x, "data.y": data.y } }, {}, function( err, num ){
				console.log('io:move (err num)', err, num );
				io.emit('moved', data);
			});
		});

		// update note content
		socket.on('update', function( data ){
			console.log('io:update', data );
			db.update({ _id: data.id }, {$set: {"data.content": data.content } }, {}, function( err, num ){
				console.log('io:update (err num)', err, num );
			});
		});

		// create note
		socket.on('create', function( data ){
			console.log('io:create', data );
			var _id = data.id; // desktop id
			var _image = data.image;
			console.log('io:create', 'desktop id', _id );

			// get desktop order
//			db.findOne({ _id: _id }, function( err, doc ){
//				if( doc ){
//					var _order = doc.order;
					var data = {desktopId: _id, 'type': 'note', data: {x: 50, y: 50}};
					if( _image ){
						data.data.image = true;
					}
					db.insert(data, function( err, doc ){
						console.log('io:create', 'insert (err, doc)', err , doc );
						io.emit('noteCreate', doc );
					});
//				}
//			});
		});

		// delete note
		socket.on('delete', function( data ){
			console.log('io:delete', data );
			db.remove({ _id: data.id }, {}, function( err, numRemoved ){ // numRemoved = 1
				console.log('io:delete remove (err, num)', err, numRemoved );
				io.emit('deleted', data);
			});
		});

		// load all notes
		socket.on('loadNotes', function( data ){
			console.log('io:loadNotes', data );
			db.find({ type: 'note', desktopId: data.id }, function( err, notes ){
				io.emit('loadNotes', notes);
			});
		});

		// load all desktops
		socket.on('loadDesktops', function( data ){
			console.log('io:loadDesktops', data );
			db.find({ type: 'desktop' }).sort({ order: 1 }).exec(function( err, desktops ){
//				console.log('io:loadDesktops (desktops)', desktops );
				io.emit('loadDesktops', desktops);
			});
		});

		// create desktop
		socket.on('desktopAdd', function( data ){
			console.log('io:desktopAdd', data );
			// look for last added desktop
			db.findOne({ type: 'desktop' }).sort({ order: -1 }).exec(function( err, doc ){
				var order = 0;
				if( doc ){
					order = doc.order + 1;
				}else{
					console.log('desktop find order ERROR, no desktops created?');
				}
//				console.log('io:desktopAdd findOne', doc );
				var desktop = {
					'type': 'desktop',
					name: helper.getDesktopName(order),
					order: order
				};
				db.insert(desktop, function( err, doc ) {
					console.log('io:desktopAdd insert', data);
					io.emit('desktopCreate', doc);
				});

			});
		});

	});

};